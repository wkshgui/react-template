import Layout from 'components/Layout';
import loadable from 'utils/loadable';

const routes = [
  {
    path: '/login',
    exact: true,
    component: loadable(() => import('containers/Login')),
    meta: {
      isTab: false,
    }
  },
  {
    path: '/',
    component: Layout,
    children: [
      {
        path: '/',
        exact: true,
        isAuth: true,
        component: loadable(() => import('containers/Home')),
        meta: {
          isTab: false,
        }
      },
      {
        path: '*',
        component: loadable(() => import('components/NotFoundPage')),
        meta: {
          isTab: false,
        }
      },
    ],
  },
];

export default routes;
