import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { Switch } from 'react-router-dom';
import { createStructuredSelector } from 'reselect';
import injectSaga from 'utils/injectSaga';

import './index.scss';
import { Layout, Menu } from 'antd';
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  UserOutlined,
  VideoCameraOutlined,
  UploadOutlined,
} from '@ant-design/icons';

import ProvideWithRoute from 'components/ProvideWithRoute';
import saga from 'containers/App/saga';

const { Header, Sider, Content } = Layout;

class SysLayout extends React.Component {
  state = {
    collapsed: false,
  };

  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };

  render() {
    const { routes } = this.props;
    return (
      <Layout className="sys-layout">
        <Sider trigger={null} collapsible collapsed={this.state.collapsed}>
          <div className="logo" />
          <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
            <Menu.Item key="1" icon={<UserOutlined />}>
              nav 1
            </Menu.Item>
            <Menu.Item key="2" icon={<VideoCameraOutlined />}>
              nav 2
            </Menu.Item>
            <Menu.Item key="3" icon={<UploadOutlined />}>
              nav 3
            </Menu.Item>
          </Menu>
        </Sider>
        <Layout className="site-layout">
          <Header className="site-layout-background" style={{ padding: 0 }}>
            {React.createElement(this.state.collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
              className: 'trigger',
              onClick: this.toggle,
            })}
          </Header>
          <Content
            className="site-layout-background"
            style={{
              margin: '10px',
              padding: 15,
              minHeight: 280,
            }}
          >
            <Switch>
              {routes &&
                routes.map((route, i) => (
                  <ProvideWithRoute key={i} {...route} />
                ))}
            </Switch>
          </Content>
        </Layout>
      </Layout>
    );
  }
};

SysLayout.propTypes = {
  routes: PropTypes.array
};

const mapStateToProps = createStructuredSelector({});
const withSaga = injectSaga({ key: 'App',  saga });
export function mapDispatchToProps(dispatch) {
  return {};
};

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withSaga,
  withConnect,
  memo,
)(SysLayout);
